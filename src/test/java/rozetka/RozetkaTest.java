package rozetka;

import common.BaseTest;
import core.pages.rozetka.MenuTree;
import core.pages.rozetka.RozetkaMainPage;
import core.pages.rozetka.SearchResultPage;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import utils.WebDriverUtil;

//WSDL (WADL) file !!!!!

public class RozetkaTest extends BaseTest {

    private WebDriver driver = super.getDriver();
    private RozetkaMainPage testPage;

    @Before
    public void initDriver(){
        testPage = new RozetkaMainPage(driver);
        driver.get("https://rozetka.com.ua");
    }


    @Test
    public void openRozetkaTest(){
        SearchResultPage searchPage = testPage.searchByText("sony playstation 4");
        Assert.assertEquals("Title doesn't match search request!", "sony playstation 4"
                , searchPage.getSearchResultLabel());
    }


    @Test
    public void searchHardDrinks40DegreeTest() throws InterruptedException, NoSuchFieldException, IllegalAccessException {
        MenuTree menuTree = testPage.openMenuTree();
        SearchResultPage searchResultPage = menuTree.clickMenuItemByText("Алкогольные напитки и продукты");
        searchResultPage.clickResultItemByText("Крепкие напитки").clickResultItemByText("Виски");
        menuTree.setFilterByText("40%");
        Thread.sleep(5000);
        Assert.assertTrue("", WebDriverUtil.getPageElementsText(searchResultPage)
                .stream()
                .allMatch(e -> e.contains("40%")));
    }


    @After
    public void close(){
        driver.close();
        driver.quit();
    }


}

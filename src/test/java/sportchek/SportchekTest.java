package sportchek;

import common.BaseTest;
import org.apache.http.entity.ContentType;
import utils.FileUtil;
import core.api.HttpRequests;
import core.pages.sportchek.BackpackPage;
import core.pages.sportchek.ShoppingCartPage;
import core.pages.sportchek.SportchekMainPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import utils.WaitUtil;
import utils.WebDriverUtil;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

public class SportchekTest extends BaseTest{

    private final WebDriver driver = super.getDriver();
    private final SportchekMainPage mainPage = new SportchekMainPage(driver);
    private static final String BASE_URL = "https://www.sportchek.ca/";

    @Before
    public void navigateToPage(){

    }

    @Test
    public void addItemToCardTest(){
        final String pageUrl = WebDriverUtil.getNavigationUrl(BackpackPage.class, BASE_URL);
        driver.get(pageUrl);
        final BackpackPage backpackPage = new BackpackPage(driver);
        final String prodName = backpackPage.getProductDescription();
        final String prodSize = "M/L";
        final String prodQTY = "1";
        backpackPage.selectSizeByText(prodSize).selectQTYByText(prodQTY).clickAddToCart();
        ShoppingCartPage cartPage = mainPage.moveToCart();
        Assert.assertEquals("Fail to add item to cart!", prodName, cartPage.getItemDescription());
    }

    @Test
    public void totalSummTest() throws IOException {
        driver.get(BASE_URL);
        HttpRequests httpRequests = new HttpRequests(driver);
        httpRequests.post(BASE_URL + "services/sportchek/cart/entry", MediaType.APPLICATION_JSON_TYPE
                , FileUtil.fileContentToString("backpack.json"));
        driver.get(WebDriverUtil.getNavigationUrl(ShoppingCartPage.class, BASE_URL));
        ShoppingCartPage cartPage = new ShoppingCartPage(driver);
        final int quantity = 3;
        final float price = cartPage.getItemPrice();
        final float total = cartPage.getEstimatedTotal();
        cartPage.setItemQTY(quantity).clickUpdateBtn();
        WaitUtil.waitUntil(e -> total != cartPage.getEstimatedTotal(), driver, 5);
        Assert.assertEquals((price * quantity), cartPage.getEstimatedTotal(), 0.1);
    }



    @After
    public void closeBrowser(){
        driver.manage().deleteAllCookies();
        driver.close();
        driver.quit();
    }
}

package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class WaitUtil {

    private WaitUtil(){
    }

    public static void waitUntil(final Function<? super WebDriver, Boolean> condition, final WebDriver driver,
                                 long timeoutSec){
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(timeoutSec, TimeUnit.SECONDS)
                .pollingEvery(200, TimeUnit.MILLISECONDS)
                .ignoring(StaleElementReferenceException.class, NoSuchElementException.class)
                .ignoring(ElementNotInteractableException.class, ElementNotVisibleException.class);

        wait.until(condition);
    }
}

package utils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Collectors;

public class FileUtil {

    private FileUtil(){
    }

    public static String fileContentToString(String fileName){
        String result = "";
        try {
            result = Files.lines(Paths.get(Objects
                    .requireNonNull(FileUtil.class.getClassLoader().getResource(fileName)).toURI()))
                    .collect(Collectors.joining(" "));

        } catch (IOException | URISyntaxException e){
            e.printStackTrace();
        }

        return result;
    }

}

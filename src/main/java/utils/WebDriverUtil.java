package utils;


import core.common.AbstractPage;
import core.common.annotations.Convertible;
import core.common.annotations.PagePartialPath;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class WebDriverUtil {

    private WebDriverUtil(){}


    public static <T extends AbstractPage>  String getNavigationUrl(final Class<T> page, final String baseUrl){
        return Optional.ofNullable(page.getAnnotation(PagePartialPath.class))
                .map(PagePartialPath::value)
                .map(e -> String.format("%s%s", baseUrl, e))
                .get();
    }

    public static <T extends AbstractPage> List<String> getPageElementsText(final T pageInstance) throws IllegalAccessException, NoSuchFieldException {
        Class pageClass = pageInstance.getClass();
        if (pageClass.getAnnotation(Convertible.class) != null){
            Field elementsField =  pageClass.getDeclaredField("elements");
            elementsField.setAccessible(true);
            return  pageInstance.convert((List<WebElement>) elementsField.get(pageInstance));

        } else
            return new ArrayList<>();
    }



}

package core.widgets.sportchek;

import core.common.AbstractWidget;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class HeaderWidget extends AbstractWidget{

    @FindBy(xpath = "//input[@class='header-search__input ui-autocomplete-input rfk_sb' " +
            "and @data-module-type='TextFieldPlaceholder']")
    private WebElement searchBar;

    @FindBy(xpath = "//input[@class='header-search__submit button' and @type='submit']")
    private WebElement searchBtn;

}

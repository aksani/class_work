package core.pages.rozetka;


import core.common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;



public class RozetkaMainPage extends AbstractPage{

    @FindBy(xpath = "//input[@type='text' and @class='rz-header-search-input-text passive']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@class='btn-link-i js-rz-search-button' and @type='submit']")
    private WebElement searchBtn;

    @FindBy(xpath = "//a[@id='fat_menu_btn']")
    private WebElement menuBtn;


    public RozetkaMainPage(final WebDriver webDriver){
        super(webDriver);
    }

    public SearchResultPage searchByText(final String text){
        searchField.sendKeys(text);
        searchBtn.click();
        return new SearchResultPage(super.getDriver());
    }

    public MenuTree openMenuTree(){
        menuBtn.click();
        return new MenuTree(super.getDriver());
    }
}

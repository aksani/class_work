package core.pages.rozetka;

import core.common.AbstractPage;
import core.common.annotations.Convertible;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

@Convertible
public class SearchResultPage extends AbstractPage {

    @FindBy(xpath = "//div[@class='g-i-tile-i-box-desc']//div[@class='g-i-tile-i-title clearfix']/a")
    private List<WebElement> elements;

    @FindBy(xpath = "//span[@id='search_result_title_text']")
    private WebElement searchTitle;

    private static final String RESULT_ITEM_XPATH = "//a[contains(text(),'%s') and (contains(@class,'sprite-side')" +
            " or contains(@class,'pab'))]";
    private static final String ALL_SEARCH_RESULTS_TEXT_XPATH = "//div[@class='g-i-tile-i-box-desc']" +
            "//div[@class='g-i-tile-i-title clearfix']/a";

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public String getSearchResultLabel(){
        return searchTitle.getText();
    }



    public SearchResultPage clickResultItemByText(String text){
        WebElement element = getDriver().findElement(By.xpath(String.format(RESULT_ITEM_XPATH, text)));
        element.click();
        return this;
    }

    public boolean isAllSearchResultsContainText(String text){
        List<WebElement> listOfElemsText = getDriver().findElements(By.xpath(ALL_SEARCH_RESULTS_TEXT_XPATH));
        return listOfElemsText.stream().allMatch(e -> e.getText().contains(text));
    }
}

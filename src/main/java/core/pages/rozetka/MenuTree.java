package core.pages.rozetka;


import core.common.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MenuTree extends AbstractPage {

    private static final String MENU_ITEM_XPATH = "//a[@name = 'fat_menu_link' and @data-level='level1' and contains(text(),'%s')]";
    private static final String FILTER_ITEM_XPATH = "//i[@class='filter-parametrs-i-l-i-default-title' and text()='%s']";


    public MenuTree(WebDriver webDriver){
        super(webDriver);
    }

    public SearchResultPage clickMenuItemByText(String text){
        WebElement element = getDriver().findElement(By.xpath(String.format(MENU_ITEM_XPATH, text)));
        element.click();
        return new SearchResultPage(super.getDriver());
    }

    public void setFilterByText(String filterItemText){
        WebElement element = getDriver().findElement(By.xpath(String.format(FILTER_ITEM_XPATH, filterItemText)));
        element.click();
    }
}

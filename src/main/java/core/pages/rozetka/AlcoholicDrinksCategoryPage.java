package core.pages.rozetka;

import core.common.AbstractPage;
import core.common.annotations.PagePartialPath;
import org.openqa.selenium.WebDriver;

@PagePartialPath(value = "/alkoholnie-napitki-i-produkty/c4626923/")
public class AlcoholicDrinksCategoryPage extends AbstractPage{

    public AlcoholicDrinksCategoryPage(WebDriver driver) {
        super(driver);
    }
}

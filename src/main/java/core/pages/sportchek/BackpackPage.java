package core.pages.sportchek;


import core.common.AbstractPage;
import core.common.annotations.PagePartialPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

@PagePartialPath(value = "product/332237753.html#332237753=332237756")
public class BackpackPage extends AbstractPage{

    @FindBy(xpath = "//main[@id='main-content']//div[@class='product-detail__title']/h1")
    private WebElement prodDescriptionText;

    @FindBy(xpath = "//div[@class='product-detail__price ']//span[@class='product-detail__price-text']")
    private WebElement prodPriceText;

    @FindBy(xpath = "//form[@id='product-detail-form']//div[@class='product-detail__color']/h3")
    private WebElement prodColourText;

    @FindBy(xpath = "//form[@id='product-detail-form']//select[@data-control-type='size']")
    private WebElement sizeSelect;

    @FindBy(xpath = "//form[@id='product-detail-form']//select[@name='quantity']")
    private WebElement quantitySelect;

    @FindBy(xpath = "//form[@id='product-detail-form']//button[@type='button' and @data-module-type='SafetyAndWarranty']")
    private WebElement addToCartBtn;


    public BackpackPage(WebDriver driver) {
        super(driver);
    }

    public String getProductDescription(){
        return this.prodDescriptionText.getText();
    }

    public float getProductPrice(){
       return Float.parseFloat(this.prodPriceText.getText().substring(1));
    }

    public String getProductColour(){
        return this.prodColourText.getText();
    }

    public BackpackPage selectSizeByText(final String size){
        Select select = new Select(this.sizeSelect);
        select.selectByVisibleText(size);
        return this;
    }

    public BackpackPage selectQTYByText(final String quantity){
        Select select = new Select(this.quantitySelect);
        select.selectByVisibleText(quantity);
        return this;
    }

    public BackpackPage clickAddToCart(){
        this.addToCartBtn.click();
        return this;
    }
}

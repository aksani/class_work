package core.pages.sportchek;

import core.common.AbstractPage;
import core.widgets.sportchek.FilterWidget;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage extends AbstractPage{

    @FindBy(xpath = "//div[@class='facets-side' and @data-module-type='Facets']")
    private FilterWidget filter;

    public SearchResultPage(final WebDriver driver) {
        super(driver);
    }
}

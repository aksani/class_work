package core.pages.sportchek;


import core.common.AbstractPage;
import core.common.annotations.PagePartialPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@PagePartialPath(value = "shopping-cart.html")
public class ShoppingCartPage extends AbstractPage{

    // Item box
    @FindBy(xpath = "//section[@class='sc-product']//div[@class='media-box__content']//a[@class='sc-product__title-link']")
    private WebElement itemDescriptionText;

    @FindBy(xpath = "//section[@class='sc-product']" +
            "//div[@class='media-box__content']//span[@class='sc-product__property__value' and ./span[text()='Colour:']]")
    private WebElement itemColourText;

    @FindBy(xpath = "//section[@class='sc-product']" +
            "//div[@class='media-box__content']//span[@class='sc-product__property__value' and ./span[text()='Size:']]")
    private WebElement itemSizeText;

    @FindBy(xpath = "//section[@class='sc-product']" +
            "//div[@class='sc-product__column sc-product__column_qty']/input[@type='text']")
    private WebElement itemQuantityInput;

    @FindBy(xpath = "//section[@class='sc-product']" +
            "//div[@class='sc-product__column sc-product__column_qty']/div/a[text()='Update']")
    private WebElement updateBtn;

    @FindBy(xpath = "//section[@class='sc-product']" +
            "//div[@class='sc-product__column sc-product__column_price']//span[@class='sc-product__price']")
    private WebElement itemPriceTextField;

    @FindBy(xpath = "//section[@class='sc-product']" +
            "//div[@class='sc-product__column sc-product__column_total']//span[@class='sc-product__price ']")
    private WebElement itemTotalTextField;

    // Order Summary box
    @FindBy(xpath = "//section[@class='co-sidebar']" +
            "//div[@class='co-sidebar__line-item co-sidebar__line-item_total']/span[@class='co-sidebar__line-item__amount']")
    private WebElement estimatedTotalField;

    // Constructor
    public ShoppingCartPage(final WebDriver driver) {
        super(driver);
    }


    // Item box
    public String getItemDescription(){
        return this.itemDescriptionText.getText();
    }

    public String getItemColour(){
        return this.itemColourText.getText();
    }

    public String getItemSize(){
        return this.itemSizeText.getText();
    }

    public ShoppingCartPage setItemQTY(final int quantity){
        this.itemQuantityInput.clear();
        this.itemQuantityInput.sendKeys(String.valueOf(quantity));
        return this;
    }

    public int getItemQTY(){
       return Integer.parseInt(this.itemQuantityInput.getAttribute("value"));
    }

    public ShoppingCartPage clickUpdateBtn(){
        this.updateBtn.click();
        return this;
    }

    public boolean isUpdateBtnActive(){
        return this.updateBtn.getAttribute("class").contains("active");
    }

    public float getItemPrice(){
       return Float.parseFloat(this.itemPriceTextField.getText().substring(1));
    }

    public float getItemTotal(){
        return Float.parseFloat(this.itemTotalTextField.getText().substring(1));
    }

    // Order Summary box
    public float getEstimatedTotal(){
        return Float.parseFloat(this.estimatedTotalField.getText().substring(1));
    }

}

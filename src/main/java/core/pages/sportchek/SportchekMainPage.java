package core.pages.sportchek;


import core.common.AbstractPage;
import core.widgets.sportchek.HeaderWidget;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

public class SportchekMainPage extends AbstractPage{

    @FindBy(xpath = "//header[@class='page-header' and @data-module-type='Header']")
    private HeaderWidget header;

    @FindBy(xpath = "//div[@class='page-header__content container']" +
            "//a[@class='header-cart__trigger drawer-ui__toggle' and @data-action='viewCart']")
    private WebElement cartIcon;


    public SportchekMainPage(WebDriver driver) {
        super(driver);
    }

    public ShoppingCartPage moveToCart(){
        this.cartIcon.click();
        return new ShoppingCartPage(super.getDriver());
    }

}

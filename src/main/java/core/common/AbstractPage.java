package core.common;


import core.impl.ExtendedFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;


public abstract class AbstractPage {

    private WebDriver driver;

    protected AbstractPage(final WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(new ExtendedFieldDecorator(driver), this);
    }

    protected WebDriver getDriver(){
        return this.driver;
    }

    public List<String> convert(List<WebElement> listOfElements){
        return listOfElements.stream().map(WebElement::getText).collect(Collectors.toList());
    }





}

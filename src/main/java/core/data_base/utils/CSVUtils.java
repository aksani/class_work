package core.data_base.utils;

import com.opencsv.CSVReader;
import core.data_base.CSVObjectDTO;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CSVUtils {

    private CSVUtils(){}


    public static <T extends CSVObjectDTO> List<T> createObjectsFromCSV(final File csvFile, final Class<T> csvObjectClass) {
        List<T> result = new ArrayList<>();

       try(CSVReader reader = new CSVReader(new FileReader(csvFile), ';')){
           List<String[]> csvRows = reader.readAll();
           String[] columnsHeaders = csvRows.get(0);
           for (int i = 1; i < csvRows.size(); i++){
               T instance = csvObjectClass.newInstance();
               String[] row = csvRows.get(i);
               for (int j = 0; j < columnsHeaders.length; j++){
                   Field field = csvObjectClass.getDeclaredField(columnsHeaders[j]);
                   field.setAccessible(true);
                   field.set(instance, row[j]);
               }
               result.add(instance);
           }
       } catch (IOException e){
           System.out.printf("Could not read file %s\n", csvFile.getPath());
       } catch (IllegalAccessException | InstantiationException | NoSuchFieldException e){
           e.printStackTrace();
       }

        return result;
    }


}

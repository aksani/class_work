package core.data_base;


import core.data_base.utils.CSVUtils;

import java.io.File;
import java.util.List;

/**
 * For test purpose.
 */
public class MainTestClass {

    public static void main(String[] args) {

        File csvFile = new File(CSVUtils.class.getClassLoader().getResource("data_base.csv").getPath());

        List<CSVUser> objs = CSVUtils.createObjectsFromCSV(csvFile, CSVUser.class);

        objs.forEach(e -> System.out.printf("ID: %s; Name: %s; Email: %s; Phone: %s.\n"
                , e.getId(), e.getName(), e.getEmail(), e.getPhone()));

    }
}
